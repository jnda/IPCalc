# IPCalc
Cette application permet de calculer des informations en lien avec une adresse IP.
* Adresse de broadcast
* L'IP minimum
* L'IP maximum 
* Le nombre d'ip en fonction du masque de sous réseau.

Les informations sont également disponibles en binaire

Il est également possible d'obtenir des informations en fonction d'un webservice https://api.ipgeolocation.io/

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/fr.jnda.android.ipcalc/)