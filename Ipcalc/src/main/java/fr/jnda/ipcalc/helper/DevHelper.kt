package fr.jnda.ipcalc.helper

import android.util.Log
import com.github.veqryn.net.Cidr4

fun Cidr4.show(){
    Log.d("DEV","=> ${this.netmask}")
    Log.d("DEV","=> ${this.addressRange}")
    Log.d("DEV","=> ${this.binaryNetmask}")
    Log.d("DEV","=> ${this.cidrSignature}")
    Log.d("DEV","=> ${this.maskBits}")
    Log.d("DEV","=> ${this.getAddressCount(true)}")
    Log.d("DEV","=> ${this.getAddressCount(false)}")
    Log.d("DEV","=> ${this.getHighAddress(true)}")
    Log.d("DEV","=> ${this.getHighAddress(false)}")
    Log.d("DEV","=> ${this.getHighBinaryInteger(true)}")
    Log.d("DEV","=> ${this.getHighBinaryInteger(false)}")
    Log.d("DEV","=> ${this.getHighCidr(true)}")
    Log.d("DEV","=> ${this.getHighCidr(false)}")
    Log.d("DEV","=> ${this.getHighIp(true)}")
    Log.d("DEV","=> ${this.getHighIp(false)}")
    Log.d("DEV","=> ${this.getLowAddress(true)}")
    Log.d("DEV","=> ${this.getLowAddress(false)}")
    Log.d("DEV","=> ${this.getLowBinaryInteger(true)}")
    Log.d("DEV","=> ${this.getLowBinaryInteger(false)}")
    Log.d("DEV","=> ${this.getLowCidr(true)}")
    Log.d("DEV","=> ${this.getLowCidr(false)}")
    Log.d("DEV","=> ${this.getLowIp(true)}")
    Log.d("DEV","=> ${this.getLowIp(false)}")
    Log.d("DEV","=> ${this.getLowBinaryInteger(false)}")
    Log.d("DEV","=> ${this.getLowBinaryInteger(false)}")
    Log.d("DEV","=> ${this.getLowBinaryInteger(false)}")
}