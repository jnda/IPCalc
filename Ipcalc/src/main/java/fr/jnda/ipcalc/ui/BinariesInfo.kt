package fr.jnda.ipcalc.ui


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.veqryn.net.Cidr4
import com.github.veqryn.net.Ip4
import fr.jnda.ipcalc.databinding.FragmentBinariesInfoBinding
import fr.jnda.ipcalc.MainActivity
import fr.jnda.ipcalc.helper.toSpannedText
import java.util.*
import fr.jnda.ipcalc.R


class BinariesInfo : BaseFragment() {

    private lateinit var rootView: View
    private lateinit var mainActivity: MainActivity
    private lateinit var binding: FragmentBinariesInfoBinding
    private lateinit var ipAddress: TextView
    private lateinit var subnetAddress: TextView
    private lateinit var networkAddress: TextView
    private lateinit var broadcastAddress: TextView
    private lateinit var hostMin: TextView
    private lateinit var hostMax: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentBinariesInfoBinding.inflate(layoutInflater)
        ipAddress = binding.idIpAddress
        subnetAddress = binding.idSubnetAddress
        networkAddress = binding.idNetworkAddress
        broadcastAddress = binding.idBroadcastAddress
        hostMin = binding.idHostMin
        hostMax = binding.idHostMax

        val view = binding.root
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Activity){
            mainActivity = context as MainActivity
        }
    }

    override fun displayInfo(cidr4: Cidr4, address: String) {

        ipAddress.text = address.toBinariesString().toSpannedText(getString(R.string.address_ip))
        subnetAddress.text = cidr4.netmask.toString().toBinariesString().toSpannedText(getString(R.string.address_subnet))
        networkAddress.text = cidr4.cidrSignature.cidrSignatureToIp().toBinariesString().toSpannedText(getString(R.string.address_network))
        broadcastAddress.text = cidr4.getHighAddress(true).toBinariesString().toSpannedText(getString(R.string.address_broadcast))
        hostMin.text = cidr4.getLowAddress(false).toBinariesString().toSpannedText(getString(R.string.address_hostmin))
        hostMax.text = cidr4.getHighAddress(false).toString().toBinariesString().toSpannedText(getString(R.string.address_hostmax))
    }

    private fun String.toBinariesString() :String {
        val ip = Ip4(this)
        val builder = StringBuilder()
        for (b in ip.inetAddress.address) {
            builder.append(String.format(Locale.FRANCE,"%08d", Integer.toBinaryString(b.toInt().and(0xff)).toInt())).append(" . ")
        }
        return builder.deleteCharAt(builder.length-2).toString()
    }


    override fun clearInfo(){
        ipAddress.text = ""
        subnetAddress.text = ""
        networkAddress.text = ""
        broadcastAddress.text = ""
        hostMin.text = ""
        hostMax.text = ""
    }

    private fun String.cidrSignatureToIp():String{
        return this.substring(0,this.indexOf("/"))
    }
}
