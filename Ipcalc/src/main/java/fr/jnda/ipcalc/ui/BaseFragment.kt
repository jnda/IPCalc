package fr.jnda.ipcalc.ui

import androidx.fragment.app.Fragment
import com.github.veqryn.net.Cidr4

abstract class BaseFragment : Fragment(){
    abstract fun displayInfo(cidr4: Cidr4, address: String)
    abstract fun clearInfo()
}