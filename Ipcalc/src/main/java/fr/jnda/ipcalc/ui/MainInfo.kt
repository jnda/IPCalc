package fr.jnda.ipcalc.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.veqryn.net.Cidr4
import com.github.veqryn.net.Ip4
import fr.jnda.ipcalc.databinding.MainInfoBinding
import fr.jnda.ipcalc.R
import fr.jnda.ipcalc.helper.toSpannedText
import java.net.UnknownHostException


class MainInfo : BaseFragment() {

    private lateinit var binding: MainInfoBinding
    private lateinit var ipAddress: TextView
    private lateinit var subnetAddress: TextView
    private lateinit var networkAddress: TextView
    private lateinit var broadcastAddress: TextView
    private lateinit var hostMin: TextView
    private lateinit var hostMax: TextView
    private lateinit var hostCount: TextView
    private lateinit var addressClass: TextView
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = MainInfoBinding.inflate(layoutInflater)
        ipAddress = binding.idIpAddress
        subnetAddress = binding.idSubnetAddress
        networkAddress = binding.idNetworkAddress
        broadcastAddress = binding.idBroadcastAddress
        hostMin = binding.idHostMin
        hostMax = binding.idHostMax
        hostCount = binding.idHostCount
        addressClass = binding.idAddressClass
        clearInfo()
        val view = binding.root
        return view
    }

    override fun displayInfo(cidr4: Cidr4,address: String){

        ipAddress.text =  address.toSpannedText(getString(R.string.address_ip))
        subnetAddress.text = cidr4.netmask.toString().toSpannedText(getString(R.string.address_subnet))
        networkAddress.text = cidr4.cidrSignature.toString().toSpannedText(getString(R.string.address_network))
        broadcastAddress.text = cidr4.getHighAddress(true).toSpannedText(getString(R.string.address_broadcast))
        hostMin.text = cidr4.getLowAddress(false).toSpannedText(getString(R.string.address_hostmin))
        hostMax.text = cidr4.getHighAddress(false).toSpannedText(getString(R.string.address_hostmax))
        hostCount.text = cidr4.getAddressCount(false).toString().toSpannedText(getString(R.string.address_hostcount))
        addressClass.text = getClassOfIp(address).toSpannedText(getString(R.string.address_class))
    }

    override fun clearInfo(){

        ipAddress.text = ""
        subnetAddress.text = ""
        networkAddress.text = ""
        broadcastAddress.text =""
        hostMin.text = ""
        hostMax.text = ""
        hostCount.text = ""
        addressClass.text = ""
    }

    private fun getClassOfIp(ip: String): String {
        //  displayBinaryAddress(ip)
        val classeA = Cidr4("10.0.0.0/8")
        val classeB = Cidr4("172.16.0.0/12")
        val classeC = Cidr4("192.168.0.0/16")
        val classeLo = Cidr4("127.0.0.0/8")

        val myIP1 = Ip4(ip)

        when {
            classeA.isInRange(myIP1, true) -> return getString(R.string.lbl_classeprivA)
            classeB.isInRange(myIP1, true) -> return getString(R.string.lbl_classeprivB)
            classeC.isInRange(myIP1, true) -> return getString(R.string.lbl_classeprivC)
            classeLo.isInRange(myIP1, true) -> return getString(R.string.lbl_classeLo)
            else -> {
                try {
                    val b = myIP1.inetAddress.address

                    val s1 = String.format("%8s", Integer.toBinaryString(b[0].toInt().and(0xff))).replace(' ', '0')
                    when {
                        s1.startsWith("0") -> return getString(R.string.lbl_classeA)
                        s1.startsWith("10") -> return getString(R.string.lbl_classeB)
                        s1.startsWith("110") -> return getString(R.string.lbl_classeC)
                        s1.startsWith("1110") -> return getString(R.string.lbl_classeD)
                        s1.startsWith("1111") -> return getString(R.string.lbl_classeE)
                    }
                } catch (e: UnknownHostException) {
                    e.printStackTrace()
                }
                return getString(R.string.lbl_indetermine)
            }
        }
    }
}