package fr.jnda.ipcalc.ui


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri.parse
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.github.veqryn.net.Cidr4
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import fr.jnda.ipcalc.R
import fr.jnda.ipcalc.databinding.FragmentLocationInfoBinding
import fr.jnda.ipcalc.helper.toSpannedText
import fr.jnda.ipcalc.network.NetworkRequest
import fr.jnda.ipcalc.poko.GeolocalisationResponse
import org.json.JSONException
import org.json.JSONObject


class LocationInfo : BaseFragment(),NetworkRequest.NetworkCallback {

    private lateinit var networkRequest: NetworkRequest
    private lateinit var rootView: View
    private lateinit var mContext: Context
    private lateinit var binding: FragmentLocationInfoBinding

    private lateinit var progress: ProgressBar
    private lateinit var countryFlag: ImageView
    private lateinit var locationIp: TextView
    private lateinit var locationContinent: TextView
    private lateinit var locationCountryCode: TextView
    private lateinit var locationCountryName: TextView
    private lateinit var locationCountryCapital: TextView
    private lateinit var locationProvince: TextView
    private lateinit var locationOrganization: TextView
    private lateinit var locationIsp: TextView
    private lateinit var locationZipcode: TextView
    private lateinit var locationCity: TextView
    private lateinit var locationDistrict: TextView
    private lateinit var locationButton: MaterialButton
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = FragmentLocationInfoBinding.inflate(layoutInflater)
        progress = binding.progressBar
        countryFlag = binding.idCountryFlags
        locationIp = binding.idLocationIp
        locationContinent = binding.idLocationContinentName
        locationCountryCode = binding.idLocationCountryCode
        locationCountryName = binding.idLocationCountryName
        locationCountryCapital = binding.idLocationCountryCapital
        locationProvince = binding.idLocationProvince
        locationOrganization = binding.idLocationOrganization
        locationIsp = binding.idLocationIsp
        locationZipcode = binding.idLocationZipcode
        locationCity = binding.idLocationCity
        locationDistrict = binding.idLocationDistrict
        locationButton = binding.idLocationMap
        rootView = binding.root
        return rootView
    }

    override fun displayInfo(cidr4: Cidr4, address: String) {
        if (isConnected()) {
            showProgress(true)
            networkRequest.addRequest(address)
        }
        else {
            Snackbar.make(rootView, getString(R.string.info_not_connected), Snackbar.LENGTH_LONG).show()
            clearInfo()
        }
    }

    private fun showProgress(state:Boolean){
        progress.visibility = if(state) View.VISIBLE else View.GONE
    }

    private fun isConnected(): Boolean{
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    override fun clearInfo() {

        countryFlag.setImageResource(android.R.color.transparent)
        locationIp.text = ""
        locationContinent.text = ""
        locationCountryCode.text = ""
        locationCountryName.text = ""
        locationCountryCapital.text = ""
        locationProvince.text = ""
        locationOrganization.text = ""
        locationIsp.text = ""
        locationZipcode.text = ""
        locationCity.text = ""
        locationDistrict.text = ""
        locationButton.visibility = View.GONE
    }

    private fun showResponse(location: GeolocalisationResponse?){
        location?.run {
            if(country_flag.isNotEmpty()) {
                Glide.with(this@LocationInfo).load(location.country_flag).into(countryFlag)
            }
            if(ip.isNotEmpty()){
                locationIp.text = ip
            }
            if(continent_name.isNotEmpty()){
                locationContinent.visibility = View.VISIBLE
                val continent = continent_name + if(continent_code.isNotEmpty()) " ( $continent_code )" else ""
                locationContinent.text = continent.toSpannedText(getString(R.string.location_continent))
            } else {
                locationContinent.visibility = View.GONE
            }
            if(country_code2.isNotEmpty()){
                locationCountryCode.visibility = View.VISIBLE
                locationCountryCode.text = country_code2.toSpannedText(getString(R.string.location_code))
            }else {
                locationCountryCode.visibility = View.GONE
            }
            if (country_name.isNotEmpty()){
                locationCountryName.visibility = View.VISIBLE
                locationCountryName.text = country_name.toSpannedText(getString(R.string.location_name))
            } else {
                locationCountryName.visibility = View.GONE
            }
            if (country_capital.isNotEmpty()){
                locationCountryCapital.visibility = View.VISIBLE
                locationCountryCapital.text = country_capital.toSpannedText(getString(R.string.location_capital))
            } else {
                locationCountryCapital.visibility = View.GONE
            }
            if (state_prov.isNotEmpty()){
                locationProvince.visibility = View.VISIBLE
                locationProvince.text = state_prov.toSpannedText(getString(R.string.location_province))
            } else {
                locationProvince.visibility = View.GONE
            }
            if (district.isNotEmpty()){
                locationDistrict.visibility = View.VISIBLE
                locationDistrict.text = district.toSpannedText(getString(R.string.location_district))
            } else {
                locationDistrict.visibility = View.GONE
            }
            if (city.isNotEmpty()){
                locationCity.visibility = View.VISIBLE
                locationCity.text = city.toSpannedText(getString(R.string.location_city))
            } else {
                locationCity.visibility = View.GONE
            }
            if (zipcode.isNotEmpty()){
                locationZipcode.visibility = View.VISIBLE
                locationZipcode.text = zipcode.toSpannedText(getString(R.string.location_zipcode))
            } else {
                locationZipcode.visibility = View.GONE
            }
            if (isp.isNotEmpty()){
                locationIsp.visibility = View.VISIBLE
                locationIsp.text = isp.toSpannedText(getString(R.string.location_isp))
            } else {
                locationIsp.visibility = View.GONE
            }
            if (organization.isNotEmpty()){
                locationOrganization.visibility = View.VISIBLE
                locationOrganization.text = organization.toSpannedText(getString(R.string.location_organisation))
            } else {
                locationOrganization.visibility = View.GONE
            }
            if (latitude.isNotEmpty() && longitude.isNotEmpty()){
                locationButton.visibility = View.VISIBLE
                locationButton.setOnClickListener {
                    val gmmIntentUri = parse("geo:$latitude,$longitude")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    if (mapIntent.resolveActivity(mContext.packageManager) != null) {
                        startActivity(mapIntent)
                    }else{
                        Snackbar.make(rootView, R.string.no_gmaps, Snackbar.LENGTH_SHORT).show()
                    }
                }
            } else {
                locationButton.visibility = View.GONE
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        networkRequest = NetworkRequest(context)
        networkRequest.addNetworkCallbackListener(this)
    }

    override fun onDetach() {
        super.onDetach()
        networkRequest.delNetworkCallbackListener(this)
    }

    override fun onSuccess(response: JSONObject) {
        showProgress(false)
        val locationResponse = Gson().fromJson(response.toString(), GeolocalisationResponse::class.java)
        Log.d("Location","Response $locationResponse")
        showResponse(locationResponse)
    }

    override fun onError(erroCode: Int, response: JSONObject) {
        clearInfo()
        showProgress(false)
        val message:String = try {
            response.get("message").toString()
        } catch (e: JSONException){
            getString(R.string.error_api)
        }
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
        Log.d("Location","Erreur $erroCode -- $response")
    }
}
