package fr.jnda.ipcalc

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import com.github.veqryn.net.Cidr4
import com.github.veqryn.net.Ip4
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.textfield.TextInputLayout
import fr.jnda.ipcalc.databinding.ActivityV2Binding
import fr.jnda.ipcalc.helper.IPV4_PATTERN
import fr.jnda.ipcalc.helper.IPV6_STD_PATTERN
import fr.jnda.ipcalc.ui.*


class MainActivity :AppCompatActivity() {

    private lateinit var binding: ActivityV2Binding
    private lateinit var seekBar2: SeekBar
    private lateinit var ipAddress: TextView
    private lateinit var subnetHelp: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isNightMode = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_key_dark_style",false)
        binding = ActivityV2Binding.inflate(layoutInflater)
        seekBar2 = binding.seekBar2
        ipAddress = binding.idIpAddress
        subnetHelp = binding.subnetHelp
        
        delegate.localNightMode = if (isNightMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO

        val view = binding.root
        setContentView(view)

        val navView: BottomNavigationView = binding.navView

        navView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_info -> {     supportFragmentManager.beginTransaction().replace(R.id.id_main_content, MainInfo()).commitNowAllowingStateLoss()}
                R.id.navigation_binaries -> { supportFragmentManager.beginTransaction().replace(R.id.id_main_content, BinariesInfo()).commitNowAllowingStateLoss() }
                R.id.navigation_location -> { supportFragmentManager.beginTransaction().replace(R.id.id_main_content, LocationInfo()).commitNowAllowingStateLoss() }
                R.id.navigation_settings -> { supportFragmentManager.beginTransaction().replace(R.id.id_main_content, AppInfo()).commitNowAllowingStateLoss() }
            }
            hideKeyboard(this@MainActivity)
            displayInfo()

            true
        }

        seekBar2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                displayInfo()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })
        ipAddress.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                displayInfo()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })
        subnetHelp.helperText = String.format(getString(R.string.lbl_subnet),"","")
        supportFragmentManager.beginTransaction().replace(R.id.id_main_content, MainInfo()).commitNow()

    }



    private fun displayInfo(){
        val address = ipAddress.text.toString()
        val fragment = supportFragmentManager.findFragmentById(R.id.id_main_content)

        when {
            IPV4_PATTERN.matcher(address).matches() -> {
                subnetHelp.helperText = String.format(getString(R.string.lbl_subnet),getCidr4(address).netmask,seekBar2.progress.toString())
                seekBar2.isEnabled = true

                fragment?.let {
                    if (fragment is BaseFragment)
                        fragment.displayInfo(getCidr4(address),address)
                }
            }
            IPV6_STD_PATTERN.matcher(address).matches() -> {
                subnetHelp.helperText = String.format(getString(R.string.lbl_subnet),"","")
                seekBar2.isEnabled = false
                Toast.makeText(this@MainActivity,getString(R.string.ipv6_not_supported),Toast.LENGTH_LONG).show()
            }
            else -> {
                subnetHelp.helperText = String.format(getString(R.string.lbl_subnet),"","")
                seekBar2.isEnabled = false
                fragment?.let {
                    if (fragment is BaseFragment)
                        fragment.clearInfo()
                }
            }
        }
    }

    private fun getCidr4(address: String):Cidr4{
        val myIP2 = Ip4(address)
        return myIP2.getLowestContainingCidr(seekBar2.progress)
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = activity.currentFocus?:View(activity)
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}